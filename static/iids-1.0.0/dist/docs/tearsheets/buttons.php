<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Components</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- For third-generation iPad with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../components/brandkit/favicon/favicon-144px.png">

    <!-- For iPhone with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../components/brandkit/favicon/favicon-114px.png">

    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../components/brandkit/favicon/favicon-72px.png">

    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="../../components/brandkit/favicon/favicon.png">

    <link rel="icon" href="../../components/brandkit/favicon/favicon.ico">
    <link href="../../components/prettify/prettify.css" rel="stylesheet">
    <link href="../assets/css/documentation.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/documentation-responsive.css" rel="stylesheet" type="text/css">
    <link href="../../css/iids.css" rel="stylesheet" type="text/css">
    <link href="../../css/responsive.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/declarative-visualizations.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
      <link id="theme-ie" href="../../css/ie.css" rel="stylesheet" type="text/css">
    <![endif]-->

    <script src="../../components/modernizr/modernizr.js"></script>
    <script src="../../components/requirejs/require.js"></script>
    <script src="../../js/require.config.js"></script>
	<!--http://jqueryrotate.googlecode.com/svn/trunk/jQueryRotate.js -->
	

	<style>
	.customButtons
	{
		width:20px;
		margin-top:10px;
	}

	</style>
    <script type="text/javascript">
		//this is a must
      require.config({ baseUrl: '../../' });
    </script>

<div id="zoomContainer">
	<h2>Choose Zoom</h2>
	<button class="btn btn-primary " id="btnZoomNegative"> <i class="icon-user icon-white"></i></button>
	<input type="text" class="customButtons" id="zoomInput" value="10" size="20"/>
	<button class="btn btn-primary" id="btnZoomPositive"> <i class="icon-user icon-white"></i></button>
</div>

<div id="tiltContainer">
	<h2>Choose Tilt</h2>
	<button class="btn btn-primary" id="btnTiltNegative"> <i class="icon-user icon-white"></i></button>
	<input type="text" class="customButtons" id="tiltInput" value="10"/>
	<button class="btn btn-primary" id="btnTiltPositive"> <i class="icon-user icon-white"></i></button>
</div>
<!-- similarly add some control for the panning -->

<div id="status">
	<input id="statusText" type="text" value="" />
</div>

<img src="manualx.png" width="400px" height="400px" id="image">


<script>
require( ['jquery'], function($) {
 $(function() {

		$("#btnZoomNegative").on("click", function () {
			neg=parseInt($('#zoomInput').val());
			if(neg>1)
				neg--;
			else
				neg=1;	
			$('#zoomInput').val(neg);
			callServerAPI("zoom",neg);

		});


		$("#btnZoomPositive").on("click", function () {
			pos=parseInt($('#zoomInput').val());
			if(pos>20)
				pos=20;
			else
				pos++;	
			$('#zoomInput').val(pos);
			callServerAPI("zoom",pos);

		});



		$("#btnTiltNegative").on("click", function () {
			neg=parseInt($('#tiltInput').val());
			if(neg>1)
				neg--;
			else
				neg=1;	
			$('#tiltInput').val(neg);
			callServerAPI("tilt",neg);
		});


		$("#btnTiltPositive").on("click", function () {
			pos=parseInt($('#tiltInput').val());
			if(pos>20)
				pos=20;
			else
				pos++;	
			$('#tiltInput').val(pos);
			callServerAPI("tilt",pos);
		});
  });
});


function callServerAPI(operName,param)
{

  $.ajax({
		type:"GET",
		url:"operations.php",
		async:true, // This makes it possible
		data:{oper:operName,val:param},
		success:function (data) {
			
			$('#statusText').val(data);
			$('#image').attr('src',data);
		},
		 error: function (request, status, error) {
			 $('#statusText').val(request.responseText);
		 }
  });
 }

function fillData()
{
    $.ajax({
            type:"GET",
            url:"dataPoints.php",
            async:true, // This makes it possible
            //data:{filePath:fileName},
            success:function (data) {
                
            },
            error: function(request, status, error) {
             
            }
        }
    );
   // return false;

}




</script>
