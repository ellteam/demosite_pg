<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Components</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- For third-generation iPad with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../components/brandkit/favicon/favicon-144px.png">

    <!-- For iPhone with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../components/brandkit/favicon/favicon-114px.png">

    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../components/brandkit/favicon/favicon-72px.png">

    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="../../components/brandkit/favicon/favicon.png">

    <link rel="icon" href="../../components/brandkit/favicon/favicon.ico">
    <link href="../../components/prettify/prettify.css" rel="stylesheet">
    <link href="../assets/css/documentation.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/documentation-responsive.css" rel="stylesheet" type="text/css">
    <link href="../../css/iids.css" rel="stylesheet" type="text/css">
    <link href="../../css/responsive.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/declarative-visualizations.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
      <link id="theme-ie" href="../../css/ie.css" rel="stylesheet" type="text/css">
    <![endif]-->

    <script src="../../components/modernizr/modernizr.js"></script>
    <script src="../../components/requirejs/require.js"></script>
    <script src="../../js/require.config.js"></script>
    <script src="../../components/jquery/jquery.js"></script>
    <script type="text/javascript">
		//this is a must
      require.config({ baseUrl: '../../' });
    </script>
    <!-- <script src="http://localhost:8080/target/target-script-min.js"></script>
 -->
  </head>
 <dl class="documentation">
 	<dt>Example4</dt>
		<dd class="example">
		<div id="chart-line" class="chart"></div>
		</dd>
	</dt>
  <dt>Example1</dt>
	<dd class="example">
    <div id="chart-bar" class="chart"></div>
	</dd>
   </dt>
	<dt>Exampl2e</dt>
		<dd class="example">
		<div id="chart-bar-stacked" class="chart"></div>
		</dd>
	</dt>
	<dt>Example3</dt>
		<dd class="example">
		<div id="chart-pie" class="chart"></div>		
		</dd>
	</dt>
	<dt>Example4</dt>
		<dd class="example">
		<div id="chart-column-stacked" class="chart"></div>
		</dd>
	</dt>


<dt>Example5</dt>
<dd class="example">
<form class="form-search pull-right">
  <div class="input-append">
    <input type="text" class="input-medium search-query" data-filter-table="dt-dom"><button class="btn btn-icon"><i class="icon-search"></i></button>
  </div>
</form>
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" data-table-name="dt-dom">
                  <thead>
                    <tr>
                      <th class="essential">Foo</th>
                      <th class="essential">Bar</th>
                      <th class="essential">Baz</th>
                      <th class="optional">Qux</th>
                      <th>Bop</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Hello1</td>
                      <td>Hello2</td>
                      <td>Hello3</td>
                      <td>Hello4</td>
                      <td>Hello5</td>
                    </tr>
                    <tr>
                      <td>Snow1</td>
                      <td>Snow2</td>
                      <td class="outlier">Snow3</td>
                      <td>Snow4</td>
                      <td>Snow5</td>
                    </tr>
                    <tr>
                      <td>Zerp1</td>
                      <td>Zerp2</td>
                      <td>Zerp3</td>
                      <td>Zerp4</td>
                      <td>Zerp5</td>
                    </tr>
                  </tbody>
                </table>
				</dd>
</dl>

  <body>
<script type="text/javascript" charset="utf-8">

  var stockConfig = {
      title: { text: 'Stock Prices' },
      series: [],
      rangeSelector: {
        selected: 1
      },
      plotOptions: {
        series: {
          compare: 'percent'
        }
      },
      yAxis: {
        labels: {
          formatter: function() {
            return (this.value > 0 ? '+' : '') + this.value + '%';
          }
        },
        title: {
          text: 'Change'
        }
      }
  };

  
  //plot options is to add the numbers inside the columns. 
  //stacklabel to show the label at the top of the bar
  var seriesConfig = {
    title: { text: 'Monthly Average Temperature' },
    subtitle: { text: 'Source: <a href="http://WorldClimate.com">WorldClimate.com</a>' },
    chart: {
      events: {
        load: function() {
          if ($(window).width() <= 480 && this.options.chart.type !== 'bar') {
            this.xAxis[0].update({
              labels: {
                rotation: 90
              }
            });
          }

		  fillData();
        }
      }
    },
    series: [{
      name: 'Complete',
      data: [3,29,23,18,11,33,27,19,25,15,35,2]
    },
	{
       name: 'Plan ECO',
      data: [0,0,0,0,0,3,0,0,2,3,1,0]
    },
	{
       name: 'Evaluate',
      data: [0,0,0,0,0,0,1,0,0,5,8,0]
    },
	{
       name: 'Review',
      data: [0,0,0,0,0,0,0,0,0,0,0,0]
    },
	{
       name: 'Cancelled',
      data: [4,1,0,9,8,11,5,6,3,1,10,1]
    }],
    xAxis: {
      title: {
        text: 'Quarters'
      },
      categories: ["Q4'10",	"Q1'11","Q2'11","Q3'11","Q4'11","Q1'12","Q2'12","Q3'12","Q4'12","Q1'13","Q2'13","Q3'13"]
    },
    yAxis: {
      title: {
        text: 'cases'
      },
	  allowDecimals:false,
	   stackLabels: {
					enabled: true,
					style: {
						fontWeight: 'bold'						
					}
					
				}
    },
    legend:{
                align: 'right',
                x: -100,
                verticalAlign: 'top',
                y: 20,
                floating: true,
                borderColor: '#CCC',
                borderWidth: 1
                
            },
			
	plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                        }
                }
            }
  };

  /*var pieConfig = {
	plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'0%</b>';
                        }
                    }
                }
            },
    series: [{
        data: [['Jan', 29.9], ['Feb', 71.5], ['Mar', 106.4], ['Apr', 129.2], ['May', 144.0], ['Jun', 176.0], ['Jul', 135.6], ['Aug', 148.5], ['Sep', 216.4], ['Oct', 194.1], ['Nov', 95.6], ['Dec', 54.4]]
    }]
  };*/
var pieConfig = {
	chart: {
            type: 'pie'
        },
	plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
							// this.point.y will give the value instead of percentage
                            return '<b>'+ this.point.name +" ("+this.point.y+') </b>';
                        }
                    }
                }
            },
	tooltip:{
		 formatter: function() {
							// this.point.y will give the value instead of percentage
                            return '<b>'+ this.point.name +" ("+this.point.y+') </b>';
                        }
	},
    series: [{
	        data: [['rohit', 94], ['STFIRC', 59], ['BASE 20/50', 59], ['HLTS II', 38], ['POD', 30], ['BASE 2/10',33], ['Convertor 100',2], ['Others', 7]]
            //data:fillData
    }]
  };
  
var lineChartConfig={
title: { text: 'Monthly Average Temperature' },
    subtitle: { text: 'Source: <a href="http://WorldClimate.com">WorldClimate.com</a>' },
    chart: {
      events: {
        load: function() {
          if ($(window).width() <= 480 && this.options.chart.type !== 'bar') {
            this.xAxis[0].update({
              labels: {
                rotation: 90
              }
            });
          }

		  fillData();
        }
      }
    },
    series: [{
      name: 'Actual closure Time Line',
      data: [75,96,97,61]
    },
	{
       name: 'Planned closure Time Line',
      data: [21,25,30,35]
    }
	],
    xAxis: {
      title: {
        text: 'Priority'
      },
      categories: ["Critical","High","Medium","Low"]
    },
    yAxis: {
      title: {
        text: 'ECR'
      },
	  allowDecimals:false
	  
    },
	plotOptions: {
				 animation:true,
				 //required to enabkle the points
                 series: {
					marker: {
						enabled: true
					}
				}
            },
    legend:{
                align: 'right',
                x: -100,
                verticalAlign: 'top',
                y: 20,
                floating: true,
                borderColor: '#CCC',
                borderWidth: 1
                
            },
	tooltip: {
				//this will show the labels for both the lines together
                shared: true,
                crosshairs: true
            }
	
	
  };





  
  
  
  require( ['jquery', 'charts','datagrids', 'prettify', 'ge-bootstrap'], function($, charts) {
    $(function() {
      // series (monotonic)
	 //fillData(charts);
	

	 

     charts.bar('chart-bar', seriesConfig );
	 charts.barStacked('chart-bar-stacked', seriesConfig );
	 charts.columnStacked('chart-column-stacked', seriesConfig );
	 charts.line('chart-line', lineChartConfig );
	  /*
      charts.column('chart-column', seriesConfig );
     
     
      charts.area('chart-area', seriesConfig );
      charts.areaStacked('chart-area-stacked', seriesConfig);

      // scatter
      charts.scatter('chart-scatter', scatterConfig);
	*/	
      // polar
      charts.pie('chart-pie', pieConfig);
      /*charts.donut('chart-donut', pieConfig);
	  */

	  $('table[data-table-name="dt-dom"]').iidsBasicDataGrid({ 'isResponsive': true });
    });
  });


function fillData()
{
    $.ajax({
            type:"GET",
            url:"dataPoints.php",
            async:true, // This makes it possible
            //data:{filePath:fileName},
            success:function (data) {

               // chart.series[0].setData(data);
                return "[['rohit', 94], ['STFIRC', 59], ['BASE 20/50', 59], ['HLTS II', 38], ['POD', 30], ['BASE 2/10',33], ['Convertor 100',2], ['Others', 7]]";
				//alert(data);	
				/*var seriesAjaxConfig = {
				title: { text: 'Monthly Average Temperature' },
				subtitle: { text: 'Source: <a href="http://WorldClimate.com">WorldClimate.com</a>' },
				chart: {
				  events: {
					load: function() {
					  if ($(window).width() <= 480 && this.options.chart.type !== 'bar') {
						this.xAxis[0].update({
						  labels: {
							rotation: 90
						  }
						});
					  }
					}
				  }
				},
				series: [{
				  name: 'Tokyo',
				  data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
				}, {
				  name: 'London',
				  data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
				}],
				xAxis: {
				  title: {
					text: 'Month'
				  },
				  categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
				},
				yAxis: {
				  title: {
					text: 'Temperature (C)'
				  },
				   stackLabels: {
								enabled: true,
								style: {
									fontWeight: 'bold'						
								}
							}
				},
				legend:{
							align: 'right',
							x: -100,
							verticalAlign: 'top',
							y: 20,
							floating: true,
							borderColor: '#CCC',
							borderWidth: 1
							
						},
						
				plotOptions: {
							column: {
								stacking: 'normal',
								dataLabels: {
									enabled: true,
									}
							},
							bar:
							{
								stacking: 'normal',
								allowPointSelect:true,
								animation:true,
								dataLabels: {
									enabled: false,
									}
							}

						},
			  };

			charts.barStacked('chart-bar-stacked', seriesAjaxConfig );
			charts.columnStacked('chart-column-stacked', seriesAjaxConfig );
			*/
            },
            error: function(request, status, error) {
             
            }
        }
    );
   // return false;

}





</script>
 <script src="../../components/respond/respond.src.js"></script>


</body>