import math
from flask import Flask, Response, render_template
from flask import request, make_response
from flask import send_file,url_for
from flask_cors import CORS, cross_origin
import json
import requests
import os
import time
import re
import csv
import csv
import io
import pymysql
import datetime
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask import send_from_directory
from PIL import Image
import base64
from io import StringIO
from io import BytesIO
from time import gmtime, strftime



app = Flask(__name__, template_folder='templates',static_url_path='/images')
CORS(app, expose_headers='Location')

port = int(os.getenv("PORT", 64781))

photos = UploadSet('photos', IMAGES)
app.config['UPLOADED_PHOTOS_DEST'] = 'images'
configure_uploads(app, photos)

@app.route('/upload', methods=['POST'])
def upload():
    if request.method == 'POST' and 'photo' in request.files:
        fileData = photos.save(request.files['photo'])
        requestFileName=request.files['photo'].filename
        inputImagePath="images/"+requestFileName
        print(request.form['detectionType'])

        ###GAURAV CODE STARTS

        oImgNumpy = Image.open(inputImagePath).convert('L')

        ###GAURAV CODE ENDS
        #save the analyzed image
        analyzedImageName=strftime("%Y-%m-%d %H:%M:%S", gmtime())+".jpg"
        oImgNumpy.save("static/"+analyzedImageName)

        finalResult = {}
        finalResult["pampers_logo"] = 3
        finalResult["whisper_packet"] = 4
        finalResult["hns_bottle_orange"] = 5
        finalResult["pampers_packet"] = 6
        finalResult["hns_bottle_yellow"] = 7
        finalResult["hns_bottle_black"] = 1
        

        return (json.dumps({"original": photos.url(fileData),
         "analyzed": url_for('static',filename=analyzedImageName),
         "results":finalResult}))
    return render_template('demoSite.html')

@app.route("/index")
def index():
    return render_template("demoSite.html")



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port, debug=True)
